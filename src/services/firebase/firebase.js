import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
    apiKey: "AIzaSyAZ5UrUB_5ne71xlnV1SZiDxhdVzBJ-rXo",
    authDomain: "agryly-essense.firebaseapp.com",
    databaseURL: "https://agryly-essense.firebaseio.com",
    projectId: "agryly-essense",
    storageBucket: "agryly-essense.appspot.com",
    messagingSenderId: "931128754077"
  };

  if (!firebase.apps.length) {
      firebase.initializeApp(config);
  }

  const auth = firebase.auth();
  const firestore = firebase.firestore();
  firestore.settings({timestampsInSnapshots: true});

  export const googleProvider = new firebase.auth.GoogleAuthProvider();

  export {
    firestore,
    auth,
  };