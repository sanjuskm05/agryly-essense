import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  List,
  ListItem,
  InputGroup,
  Input,
  Icon,
  Card,
  CardItem,
  Body,
  Grid,
  Col
} from "native-base";
import { observer, inject } from "mobx-react/native";
import ListDropdown from "../CommonComponents/ListDropdown/index.js";
import RoundImageButton from "../CommonComponents/RoundImageButton/index.js";
import BannerSlider from "../CommonComponents/BannerSlider/index.js";
import Banner from "../CommonComponents/Banner/index.js";
import ThemeHeader from "../CommonComponents/Header/index.js";
import MyFooter from "../CommonComponents/Footer";
import commonColor from "../../theme/variables/commonColor.js";
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
import Style from "./style.js";
var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

var bannerSliderData = [
  {
    id: 1,
    bannerImageSource: require("../../images/water.jpg"),
    bannerImageText: "Water",
    bannerSmallText: "Born Essense"
  },
  {
    id: 2,
    bannerImageSource: require("../../images/milk.png"),
    bannerImageText: "Milk",
    bannerSmallText: "Build Essense"
  }
];

@inject("view.app", "domain.user", "app", "routerActions")
@observer
class HomePage extends Component {
  render() {
    const userStore = this.props["domain.user"];
    const navigation = this.props.navigation;
    return (
      <Container>
        <ThemeHeader 
          PageTitle="Essense" 
          IconLeft="menu" 
          navigation={navigation}
          />
        <Content
          padder
          contentContainerStyle={{ paddingBottom: 20 }}
          showsVerticalScrollIndicator={false}
        >
          <Card style={Style.scrollBanner}>
            <Text style={Style.bannerHeading}>
              Choose yoour essense 
            </Text>
            <List
              removeClippedSubviews={false}
              directionalLockEnabled={false}
              horizontal={false}
              showsHorizontalScrollIndicator={false}
              bounces={false}
              dataArray={bannerSliderData}
              renderRow={item =>
                <BannerSlider
                  onPress={() => alert('Add to bag')}
                  bannerImageSource={item.bannerImageSource}
                  bannerImageText={item.bannerImageText}
                  bannerSmallText={item.bannerSmallText}
                />}
            />
          </Card>
        </Content>
        <MyFooter navigation={navigation} selected={"home"} />
      </Container>
    );
  }
}
export default HomePage;
