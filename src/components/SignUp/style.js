import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

export default{
    buttonText: {
        paddingLeft: 3
      },
      button: {
        marginTop: 30,
        paddingHorizontal: 30,
        backgroundColor:'#01C398',
        borderColor:'#4267b2' 
      },
      gbutton: {
        marginTop: 30,
        backgroundColor:'#dd4b39',
        borderColor:'#4267b2' ,
        paddingHorizontal:20,
        alignSelf:'center'
      },
      gicon: {
        color:'#fff',
        marginRight: 10
      },
};
