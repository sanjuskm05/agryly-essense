import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  View,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  InputGroup,
  Input,
  Footer,
  Text,
  FooterTab,
  Icon,
  Card,
  CardItem,
  Label,
  Form,
  Item,
  Segment,
  List,
  ListItem,
  Toast
} from "native-base";
import { observer, inject } from "mobx-react/native";
import ThemeHeader from "../CommonComponents/Header/index.js";
import Style from "./style.js";
import { auth, firestore } from '../../services/firebase';
import firebase from 'firebase/app';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMI from 'react-native-vector-icons/MaterialIcons';

var deviceWidth = Dimensions.get("window").width;
import Expo from 'expo';

const INITIAL_STATE = {
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
  icEye: 'visibility-off',
  password: true,
  icEye2: 'visibility-off',
  confirmPwd: true,
};

@inject("view.app", "domain.user", "app", "routerActions")
@observer
class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <IconMCI name="account-plus" style={{ fontSize: 24, color: tintColor }} />
    )
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        console.log(user);
      }
    });
  }

  changePwdType = () => {
    let newState;
    if (this.state.password) {
      newState = {
        icEye: 'visibility',
        password: false
      }
    } else {
      newState = {
        icEye: 'visibility-off',
        password: true
      }
    }

    // set new state value
    this.setState(newState);

  };

  changeConfirmPwdType = () => {
    let newState;
    if (this.state.confirmPwd) {
      newState = {
        icEye2: 'visibility',
        confirmPwd: false
      }
    } else {
      newState = {
        icEye2: 'visibility-off',
        confirmPwd: true
      }
    }

    // set new state value
    this.setState(newState)

  };

  signup(navigation) {

    auth.doCreateUserWithEmailAndPassword(this.state.email, this.state.passwordOne)
      .then(authUser => {
        // Create a user in your own accessible Firebase Database too
        firestore.doCreateUser(authUser.user.uid, this.state.email)
          .then(() => {
            alert('Your account was created!');
            this.setState(() => ({ ...INITIAL_STATE }));
            navigation.navigate('Login');
          })
          .catch(error => {
            alert("Account creation failed: " + error.message);
          });
      })
      .catch(error => {
        alert("Account creation failed: " + error.message);
      });
  }

  async signInWithGoogle(navigation) {
    const result = await Expo.Google.logInAsync({
      iosClientId: '931128754077-fsei4c9kqi7f9fdi0n8tsdpfotdp8q09.apps.googleusercontent.com',
      androidClientId: '931128754077-3ldjv2l2dm8vp4ng87vdpk7e1at0a16i.apps.googleusercontent.com',
      scopes: ["profile", "email"]
    });

    if (result.type == 'success') {
      try {
        const credential = firebase.auth.GoogleAuthProvider.credential(null, result.accessToken);

        firebase.auth().signInAndRetrieveDataWithCredential(credential)
          .then(() => {
            firestore.doCreateUser(result.user.id, result.user.email)
              .then(() => {
                alert("Successful Google Login!");
                navigation.navigate('Profile');
              });
          });
      } catch (error) {
        console.log("Google sign-in Issue:" + error.message);
      }
    }
  }

  render() {
    const {
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '';

    const userStore = this.props["domain.user"];
    const navigation = this.props.navigation;

    return (
      <Container>
        <ThemeHeader
          PageTitle="Sign Up"
          IconLeft="arrow-back"
          route="loginHome"
          navigation={navigation}
        />
        <Content
          padder
          style={{ backgroundColor: "#fff", marginBottom: null }}
          bounces={false}
        >
          <Form>
            <Item stackedLabel underline style={{ marginLeft: 0 }}>
              <Label>E-mail</Label>
              <InputGroup>
                <IconMI name='email' size={25} color="#01C398" />
                <Input
                  label='Email'
                  value={this.state.email}
                  onChangeText={(text) => this.setState({ email: text })}
                  type="email"
                  placeholder="Email address" />
              </InputGroup>
            </Item>
            <Item stackedLabel underline style={{ marginLeft: 0 }} >
              <Label>Password</Label>
              <InputGroup>
                <Icon name='ios-unlock' />
                <Input
                  value={passwordOne}
                  onChangeText={(text) => this.setState({ passwordOne: text })}
                  type="password"
                  placeholder="Enter Password" secureTextEntry={this.state.password} />
                <IconMI
                  name={this.state.icEye}
                  size={20}
                  color="#01C398"
                  onPress={this.changePwdType}
                />
              </InputGroup>
            </Item>
            <Item stackedLabel underline style={{ marginLeft: 0 }}>
              <Label>Confirm Password</Label>
              <InputGroup>
                <Icon name='ios-unlock' />
                <Input
                  value={passwordTwo}
                  onChangeText={(text) => this.setState({ passwordTwo: text })}
                  type="password"
                  placeholder="Re-enter Password" secureTextEntry={this.state.confirmPwd} />
                <IconMI
                  name={this.state.icEye2}
                  size={20}
                  color="#01C398"
                  onPress={this.changeConfirmPwdType}
                />
              </InputGroup>
            </Item>
            {/*<Segment>
                    <Button style={{borderColor:'#777', borderWidth:0.5}} first><Text>Female</Text></Button>
                    <Button style={{borderColor:'#777', borderWidth:0}} last><Text>Male</Text></Button>
                </Segment>*/}
            <Button
              block
              rounded
              primary
              style={Style.button}
              disabled={isInvalid}
              onPress={() => this.signup(navigation)}
            >
              <IconMCI name="account-plus" size={25} />
              <Label style={Style.buttonText}>  Sign Up</Label>
            </Button>
            <Button
              rounded
              primary
              style={Style.gbutton}
              onPress={() => this.signInWithGoogle(navigation)}
            >
              <Icon name="logo-google" style={Style.gicon} />
              <Text style={Style.ftext}>Sign in with Google</Text>
            </Button>
            <Button
              transparent
              style={{ marginTop: 5, alignSelf: 'center' }}
              onPress={() => navigation.navigate('Login')}
            >
              <Label>Already have an account?</Label>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

//export default withRouter(SignUpPage);
export default SignUp;
