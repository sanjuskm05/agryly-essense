import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  View,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  List,
  ListItem,
  InputGroup,
  Input,
  Text,
  Icon,
  Card,
  CardItem,
  Label,
  Form,
  Item,
  Toast,
  Spinner
} from "native-base";
import { observer, inject } from "mobx-react/native";
import ThemeHeader from "../CommonComponents/Header/index.js";
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
import IconMI from "react-native-vector-icons/MaterialIcons";
import Style from "./style.js";
import { auth, firestore } from '../../services/firebase';
import Expo from 'expo';
import firebase from 'firebase/app';

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
  iconEye: 'visibility-off',
  pwdSecure: true,
};

const INITIAL_SOCIAL_STATE = {
  error: null,
};

@inject("view.app", "domain.user", "app", "routerActions")
@observer
class Login extends Component {

  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <IconMC name="login" style={{ fontSize: 24, color: tintColor }} />
    )
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        console.log(user);
      }
    });
  }

  changePwdType = () => {
    let newState;
    if (this.state.pwdSecure) {
      newState = {
        iconEye: 'visibility',
        pwdSecure: false
      }
    } else {
      newState = {
        iconEye: 'visibility-off',
        pwdSecure: true
      }
    }

    // set new state value
    this.setState(newState);
  };

  login(navigation) {
    const {
      email,
      password,
      pwd,
    } = this.state;

    <Spinner />

    const {
      history,
    } = this.props;

    auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        alert("Successful Login!");
        navigation.navigate('Profile');
      })
      .catch(error => {
        alert('Error in login' + error.message);
      });
  }

  async loginWithGoogle(navigation) {
    try {
      const result = await Expo.Google.logInAsync({
        iosClientId: '931128754077-fsei4c9kqi7f9fdi0n8tsdpfotdp8q09.apps.googleusercontent.com',
        androidClientId: '931128754077-3ldjv2l2dm8vp4ng87vdpk7e1at0a16i.apps.googleusercontent.com',
        scopes: ["profile", "email"]
      });

      if (result.type === 'success') {
        const credential = firebase.auth.GoogleAuthProvider.credential(null, result.accessToken);

        firebase.auth().signInAndRetrieveDataWithCredential(credential)
          .then(() => {
            alert("Successful Google Login!");
            navigation.navigate('Profile');
          })
          .catch(error => {
            alert('Firebase Google save error! ' + error.message);
          });
      }
    } catch (e) {
      console.log("Google Sign-in error: ", e);
    }

  }

  render() {
    const {
      email,
      password,
      error,
    } = this.state;

    const isInvalid =
      password === '' ||
      email === '';

    const userStore = this.props["domain.user"];
    const navigation = this.props.navigation;
    return (
      <Container>
        <ThemeHeader
          PageTitle="Login"
          IconLeft="arrow-back"
          route="loginHome"
          navigation={navigation}
        />
        <Content
          padder
          style={{ backgroundColor: "#fff", marginBottom: null }}
          bounces={false}
        >
          <Form>
            <Item underline style={{ marginLeft: 0 }}
              stackedLabel>
              <Label>E-mail</Label>
              <InputGroup>
                <IconMI name='email' size={25} color="#01C398" />
                <Input
                  value={email}
                  onChangeText={(text) => this.setState({ email: text })}
                  type="email"
                  placeholder="Email address" />
              </InputGroup>
            </Item>
            <Item stackedLabel underline style={{ marginLeft: 0 }}>
              <Label> Password </Label>
              <InputGroup>
                <Icon name='ios-lock' />
                <Input
                  value={password}
                  onChangeText={(text) => this.setState({ password: text })}
                  type="password"
                  placeholder="Password" secureTextEntry={this.state.pwdSecure} />
                <IconMI
                  name={this.state.iconEye}
                  size={20}
                  color='#01C398'
                  onPress={this.changePwdType}
                />
              </InputGroup>
            </Item>
            <Button
              block
              rounded
              primary
              disabled={isInvalid}
              onPress={() => this.login(navigation)}
              style={Style.button}
            >
              <IconMC name="login" size={25} />
              <Label> Login </Label>
            </Button>
            <Button block transparent>
              <Text style={{ fontWeight: "700", fontSize: 12 }}>
                FORGOT PASSWORD?
              </Text>
            </Button>
            <Button
              rounded
              primary
              style={Style.gbutton}
              onPress={() => this.loginWithGoogle(navigation)}
            >
              <Icon name="logo-google" style={Style.gicon} />
              <Text style={Style.ftext}>Login with Google</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
export default Login;
